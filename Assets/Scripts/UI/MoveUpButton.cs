﻿using Game;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class MoveUpButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
            => SetPressed(true);

        public void OnPointerUp(PointerEventData eventData)
            => SetPressed(false);

        private void SetPressed(bool value)
            => Player.Get().Move(value);
    }
}
