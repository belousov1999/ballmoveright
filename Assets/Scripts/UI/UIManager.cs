﻿using Enums;
using Game;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        //Editor
        [SerializeField] private GameUIElement[] _uiElements;
        [SerializeField] private Button _startButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Text endText;
        
        private void Start()
        {
            var gameStateManager = GameStateManager.Get();
            //OnGameStateChanged(gameStateManager.CurrentState);
            gameStateManager.OnGameStateChangedEvent += OnGameStateChanged;

            _startButton.onClick.AddListener(OnStartButton);
            _restartButton.onClick.AddListener(OnRestartButton);
        }

        void OnGameStateChanged(EGameStates newState)
        {
            foreach (var ui in _uiElements)
                ui.gameObject.SetActive(ui.ShowStates.Contains(newState));

            if (newState == EGameStates.STATE_LOSE)
                endText.text = "You losed";
            else if (newState == EGameStates.STATE_WIN)
                endText.text = "You Won";
        }

        void OnStartButton()
            => GameStateManager.Get().CurrentState = EGameStates.STATE_GAME;

        void OnRestartButton()
        {
            Player.Get().Restart();
            GameStateManager.Get().CurrentState = EGameStates.STATE_GAME;
        }
    }
}
