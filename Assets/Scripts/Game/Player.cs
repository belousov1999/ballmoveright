﻿using Enums;
using UnityEngine;

namespace Game
{
    public class Player : MonoBehaviour
    {
        //Editor
        [SerializeField] private RectTransform _parallaxRect;
        [SerializeField] private float playerSpeed;

        //Private
        private float _maxHeight, _minHeight, _maxWidth;
        private RectTransform _playerRect;
        private bool _moveUp, _playing;
        private Vector2 _startPosition;

        //Private Static
        private static Player _instance;

        private void Awake()
        {
            _instance = this;
        }

        /// <summary>
        /// Set high and low points, finish point and restart point
        /// Decrease a little so that there is a visible touch
        /// </summary>
        void Start()
        {
            _playerRect = (RectTransform)transform;
            _maxHeight = _parallaxRect.rect.height / 2f - _playerRect.rect.height / 3f;
            _minHeight = -_maxHeight;
            _maxWidth = Screen.width/2f;
            _startPosition = _playerRect.anchoredPosition;

            GameStateManager.Get().OnGameStateChangedEvent += OnGameStateChanged;
        }

        public static Player Get()
            => _instance;

        /// <summary>
        /// We set the variable from the outside.
        /// If true, the player is moving up.
        /// </summary>
        /// <param name="isMoveUp"></param>
        public void Move(bool isMoveUp)
            => _moveUp = isMoveUp;

        /// <summary>
        /// Restart player position
        /// </summary>
        public void Restart()
            => _playerRect.anchoredPosition = _startPosition;

        /// <summary>
        /// If we in the menu\lost\won, etc. then we must not move.
        /// We move only in the play state.
        /// </summary>
        /// <param name="newState"></param>
        void OnGameStateChanged(EGameStates newState)
        {
            _playing = newState == EGameStates.STATE_GAME;
        }

        /// <summary>
        /// We check if the player crossed line the upper or lower line,
        /// or has reached the finish 
        /// </summary>
        /// <param name="pos"></param>
        void CheckPlayerPosition(Vector2 pos)
        {
            if (pos.y >= _maxHeight || pos.y <= _minHeight)
                GameStateManager.Get().CurrentState = EGameStates.STATE_LOSE;
            else if (pos.x >= _maxWidth)
                GameStateManager.Get().CurrentState = EGameStates.STATE_WIN;
        }

        void Update()
        {
            if (!_playing) return;
            var pos = _playerRect.anchoredPosition;
            pos.x += playerSpeed * Time.deltaTime;
            pos.y += (_moveUp ? playerSpeed : -playerSpeed) * Time.deltaTime;

            _playerRect.anchoredPosition = pos;
            CheckPlayerPosition(pos);
        }

        private void OnDestroy()
        {
            _instance = null;
        }
    }
}