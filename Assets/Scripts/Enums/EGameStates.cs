﻿
namespace Enums
{
    public enum EGameStates
    {
        STATE_MENU,
        STATE_GAME,
        STATE_LOSE,
        STATE_WIN
    }
}
